# -*- coding: utf-8 -*-
from __future__ import print_function

from gilded_rose import *

if __name__ == "__main__":

    generic_items = [
            #Funcionamiento normal del item
            GenericItem(name="+5 Dexterity Vest", sell_in=10, quality=20),
            #Fecha de venta por debajo de 0, pierde el doble de calidad
            GenericItem(name="+5 Dexterity Vest", sell_in=-2, quality=20),
    ]

    # Su calidad aumenta en 1 unidad cada día
    aged_brie_items = [
        #Funcionamiento normal de aged brie
        AgedBrie(name="Aged Brie", sell_in=3, quality=2),
        #Fecha de venta por debajo de 0, gana  el doble de calidad
        AgedBrie(name="Aged Brie", sell_in=-1, quality=4),
        #Calidad no puede ser mayor a 50
        AgedBrie(name="Aged Brie", sell_in=10, quality=51),
    ]
    # El artículo Sulfuras no modifica su fecha de venta ni se degrada en calidad
    sulfuras_items = [
        #Funcionamiento normal de Sulfuras
        Sulfuras(name="Sulfuras, Hand of Ragnaros", sell_in=4, quality=5),
    ]

    backstage_passes_items = [
        #Funcionamiento normal de Backstage
        BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
        #Si faltan 10 días o menos para el concierto, la calidad se incrementa en 2 unidades
        BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=8, quality=20),
        #Si faltan 5 días o menos, la calidad se incrementa en 3 unidades
        BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=3, quality=20),
        #Luego de la fecha de venta la calidad cae a 0
        BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=1, quality=20),
    ]

    conjured_items = [
        #Los artículos conjurados degradan su calidad al doble de velocidad que los normales
        Conjured(name="Conjured Mana Cake", sell_in=3, quality=6),
        #Fecha de venta por debajo de 0, pierde el doble de calidad
        Conjured(name="Conjured Mana Cake", sell_in=-2, quality=20),
    ]

    #Ejecutamos update_quality
    items = generic_items + aged_brie_items + sulfuras_items + backstage_passes_items + conjured_items

    days = 7
    import sys
    if len(sys.argv) > 1:
        days = int(sys.argv[1]) + 1
    for day in range(days):
        print("-------- day %s --------" % day)
        print("name, sellIn, quality")
        for item in items:
            print(item)
        print("")
        GildedRose(items).update_quality()
