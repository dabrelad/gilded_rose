# -*- coding: utf-8 -*-
import unittest

from gilded_rose import *


class GildedRoseTest(unittest.TestCase):

    def test_foo(self):
        # Su calidad decrementa en 1 unidad cada día
        generic_items = [
            #Funcionamiento normal del item
            GenericItem(name="+5 Dexterity Vest", sell_in=10, quality=20),
            #Fecha de venta por debajo de 0, pierde el doble de calidad
            GenericItem(name="+5 Dexterity Vest", sell_in=-2, quality=20),
        ]

        # Su calidad aumenta en 1 unidad cada día
        aged_brie_items = [
            #Funcionamiento normal de aged brie
            AgedBrie(name="Aged Brie", sell_in=3, quality=2),
            #Fecha de venta por debajo de 0, gana  el doble de calidad
            AgedBrie(name="Aged Brie", sell_in=-1, quality=4),
            #Calidad no puede ser mayor a 50
            AgedBrie(name="Aged Brie", sell_in=10, quality=51),
        ]
        # El artículo Sulfuras no modifica su fecha de venta ni se degrada en calidad
        sulfuras_items = [
            #Funcionamiento normal de Sulfuras
            Sulfuras(name="Sulfuras, Hand of Ragnaros", sell_in=4, quality=5),
        ]

        backstage_passes_items = [
            #Funcionamiento normal de Backstage
            BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=15, quality=20),
            #Si faltan 10 días o menos para el concierto, la calidad se incrementa en 2 unidades
            BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=8, quality=20),
            #Si faltan 5 días o menos, la calidad se incrementa en 3 unidades
            BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=3, quality=20),
            #Luego de la fecha de venta la calidad cae a 0
            BackstagePasses(name="Backstage passes to a TAFKAL80ETC concert", sell_in=1, quality=20),
        ]

        conjured_items = [
            #Los artículos conjurados degradan su calidad al doble de velocidad que los normales
            Conjured(name="Conjured Mana Cake", sell_in=3, quality=6),
            #Fecha de venta por debajo de 0, pierde el doble de calidad
            Conjured(name="Conjured Mana Cake", sell_in=-2, quality=20),
        ]

        #Ejecutamos update_quality
        items = generic_items + aged_brie_items + sulfuras_items + backstage_passes_items + conjured_items
        gilded_rose = GildedRose(items)
        gilded_rose.update_quality()

        #Unit Testing
        #Generic
        self.assertEqual(19, items[0].quality)
        self.assertEqual(18, items[1].quality)
        #AgedBrie
        self.assertEqual(3, items[2].quality)
        self.assertEqual(6, items[3].quality)
        self.assertEqual(50, items[4].quality)
        #Sulfuras
        self.assertEqual(5, items[5].quality)
        #BackstagePasses
        self.assertEqual(21, items[6].quality)
        self.assertEqual(22, items[7].quality)
        self.assertEqual(23, items[8].quality)
        self.assertEqual(0, items[9].quality)
        #Conjured
        self.assertEqual(4, items[10].quality)
        self.assertEqual(16, items[11].quality)

        
if __name__ == '__main__':
    unittest.main()
