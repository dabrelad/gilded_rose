# -*- coding: utf-8 -*-

class GildedRose(object):

    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            if(item != Sulfuras):
                item.update_quality()
                

class Item:
    MAX_QUALITY = 50
    SELL_DAY = 0

    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)

class Sulfuras(Item):
    def update_quality(self):
        pass

class BackstagePasses(Item):
    def calculate_quality_change(self):
        if(self.sell_in >= 10):
            return 1
        elif(self.sell_in < 10 and self.sell_in >= 5):
            return 2
        elif(self.sell_in < 5 and self.sell_in > self.SELL_DAY):
            return 3
        else:
            return 0

    def update_quality(self):
        self.sell_in -= 1
        quality_change = self.calculate_quality_change()
        if(quality_change > 0 and self.quality + quality_change <= self.MAX_QUALITY):
            self.quality += quality_change
        elif(self.quality + quality_change > self.MAX_QUALITY):
            self.quality = self.MAX_QUALITY
        else:
            self.quality = 0

class GenericItem(Item):
    def calculate_quality_change(self):
        if(self.sell_in < self.SELL_DAY):
            return 2
        else: 
            return 1

    def update_quality(self):
        self.sell_in -= 1
        multiplier = 1
        if(Conjured == type(self)):
            multiplier = 2
        quality_change = self.calculate_quality_change() * multiplier
        if(self.quality - quality_change >= 0):
            self.quality -= quality_change
        else:
            self.quality = 0

class AgedBrie(GenericItem):
    def update_quality(self):   
        self.sell_in -= 1
        quality_change = self.calculate_quality_change()
        if(self.quality + quality_change > self.MAX_QUALITY):
            self.quality = self.MAX_QUALITY
        else:
            self.quality += quality_change


class Conjured(GenericItem):
    def update_quality(self):
        GenericItem.update_quality(self)